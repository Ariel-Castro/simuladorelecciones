var bodyParser = require("body-parser");
var mysql = require("mysql");
var fs = require("fs");

//Credenciales Base de datos
var credenciales = {
    user:"root",
    password:"",
    port:"3306",
    host:"localhost",
    database:"dbElecciones"
};

//Servidor web en nodeJS para publicar archivos estaticos.
var express = require("express");
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//Exponer una carpeta como publica, unicamente para archivos estaticos: .html, imagenes, .css, .js
app.use(express.static("public"));



var connection = mysql.createConnection(credenciales);
 connection.connect(function(error){
    if(error){
       throw error;
    }else{
       console.log('Conexion correcta.');
    }
 });

 // post para registrar una eleccion
app.post("/registrarEleccion", function(request, response){
   var conexion = mysql.createConnection(credenciales);
   const { sistema,
         depto,
         cantidadDiputados,
         nombreEleccion,
         fecha,
         censoElectoral,
         votosBlanco,
         votosNulos,
         nombrepartidos,
         siglasPartidos,
         cantidadVotos
      } = request.body;
   const sqlregistrareleccion = `INSERT INTO tbl_elecciones(
                                                            departamentoId,
                                                            sistemaEleccionId,
                                                            nombreEleccion,
                                                            fechaEleccion,
                                                            censoElectoral,
                                                            cantidadDiputados,
                                                            votosBlanco,
                                                            votosNulo
                                                      )
                                                      VALUES
                                                      (?,?,?,?,?,?,?,?);`;
   conexion.query(sqlregistrareleccion, [
      1,
      1,
      nombreEleccion,
      fecha,
      censoElectoral,
      cantidadDiputados,
      votosBlanco,
      votosNulos], function(errorInsert, dataInsert){
         if (errorInsert){ 
            throw errorInsert;
         }else{
                  var idEleccionInsertada = 0;

                  var query = conexion.query('select eleccionId as eleccionId from tbl_elecciones order by eleccionId DESC limit 1;', function(error, result){
                     if(error){
                        throw error;
                     }else{
                        
                        var resultado = result;
                        idEleccionInsertada = resultado[0].eleccionId;

                        //insertando Partidos politicos
                        var valuesArray = [];
                        var contPartidos = 0;
                        
                           nombrepartidos.forEach(function (elemento, indice, array) {
                              contPartidos +=1
                        });
                           for (var i = 0; i < contPartidos; i++) {
                                 valuesArray.push([idEleccionInsertada,nombrepartidos[i],siglasPartidos[i],cantidadVotos[i]])         
                           }     
                           
                        const sqlregistrarPartido = `INSERT INTO tbl_partidos(
                                                                           eleccionId,
                                                                           nombrePartido,
                                                                           siglasPartido,
                                                                           cantidadVotos
                                                                           )
                                                                           VALUES ?`;
                        conexion.query(sqlregistrarPartido, 
                           [valuesArray]
                           ,function(errorInsert, dataInsert){
                              if (errorInsert){ 
                                 throw errorInsert;
                              }else{
                                       
                                    response.send('bien');
                                 } 
                        });
                     }
                  });
                  
            } 
   });
});


//funcion para obtener la cantidad de diputados por partido
app.get("/obtener-cantidad-candidatos", function(request, response){

   var conexion = mysql.createConnection(credenciales);
   var idEleccionInsertada = 0;
   var idSistemaInsertado = 1;

   var query = conexion.query('select eleccionId, sistemaEleccionId from tbl_elecciones order by eleccionId DESC limit 1;', function(error, result){
      if(error){
         throw error;
      }else{
         
         var resultado = result;
         idEleccionInsertada = resultado[0].eleccionId;
         idSistemaInsertado = resultado[0].sistemaEleccionId;
             
         var sql = 'CALL SP_OBTENER_CANTIDAD_DIPUTADOS('+idEleccionInsertada+','+idSistemaInsertado+');';
         
         var cantidadDiputadosObtenidos = [];
         conexion.query(sql)
         .on("result", function(resultado){
            cantidadDiputadosObtenidos.push(resultado);
         })
         .on("end",function(){
            response.send(cantidadDiputadosObtenidos);
            console.log(cantidadDiputadosObtenidos);
       
         });   
      }
   });
});


//funcion para obtener la cantidad de diputados por residuo electoral
app.get("/obtener-cantidad-residuo", function(request, response){

   var conexion = mysql.createConnection(credenciales);
   var idEleccionInsertada = 0;
   var idSistemaInsertado = 1;

   var query = conexion.query('select eleccionId, sistemaEleccionId from tbl_elecciones order by eleccionId DESC limit 1;', function(error, result){
      if(error){
         throw error;
      }else{
         
         var resultado = result;
         idEleccionInsertada = resultado[0].eleccionId;
         idSistemaInsertado = resultado[0].sistemaEleccionId;
             
         var sql = 'CALL SP_OBTENER_DIPUTADOS_RESIDUO('+idEleccionInsertada+','+idSistemaInsertado+');';
         
         var cantidadObtenidosResiduo = [];
         conexion.query(sql)
         .on("result", function(resultado){
            cantidadObtenidosResiduo.push(resultado);
         })
         .on("end",function(){
            response.send(cantidadObtenidosResiduo);
         });   
      }
   });
});

//funcion para obtener las elecciones guardadas
app.get("/obtener-eleccion-guardadas", function(request, response){

   var conexion = mysql.createConnection(credenciales);
   var sql =   `SELECT eleccionId,nombreEleccion FROM tbl_elecciones;`;
   var eleccionesInsertadas = [];
   conexion.query(sql)
   .on("result", function(resultado){
      eleccionesInsertadas.push(resultado);
   })
   .on("end",function(){
       response.send(eleccionesInsertadas);
   });

});

//obtiene los datos del archivo
app.get("/obtener-datos-eleccion",function(request, response){
   var conexion = mysql.createConnection(credenciales);
   var sql =   `SELECT cantidadDiputados,nombreEleccion,fechaEleccion,censoElectoral,votosBlanco,votosNulo FROM tbl_elecciones WHERE eleccionId = ?;`;
   var contenidoGeneralEleccion = [];
   conexion.query(sql, 
                   [
                       request.query.eleccionId   
                   ])
   .on("result", function(resultado){
      contenidoGeneralEleccion.push(resultado);
   })
   .on("end",function(){
       response.send(contenidoGeneralEleccion);
   });
});

//obtiene los datos del archivo
app.get("/obtener-datos-partidos",function(request, response){
   var conexion = mysql.createConnection(credenciales);
   var sql =   `SELECT nombrePartido,siglasPartido,cantidadVotos FROM tbl_partidos WHERE eleccionId = ?;`;
   var contenidoPartidosEleccion = [];
   conexion.query(sql, 
                   [
                       request.query.eleccionId   
                   ])
   .on("result", function(resultado){
      contenidoPartidosEleccion.push(resultado);
   })
   .on("end",function(){
       response.send(contenidoPartidosEleccion);
   });
});

 // post para registrar una eleccion
 app.post("/registrarCandidatos", function(request, response){
   var conexion = mysql.createConnection(credenciales);
   const { 
      partidosId,
      nombreCandidatos
      } = request.body;
      var lastDepartamentoId = 0;

      var query = conexion.query('select departamentoId as departamentoId from tbl_elecciones order by eleccionId DESC limit 1;', function(error, result){
         if(error){
            throw error;
         }else{
            
            var resultado = result;
            lastDepartamentoId = resultado[0].departamentoId;
            alert(lastDepartamentoId);
            //insertando Candidatos
            var arrayCandidatos = [];
            var contCandidatos = 0;
            
            nombreCandidatos.forEach(function (elemento, indice, array) {
               contCandidatos +=1
            });
               for (var i = 0; i < contPartidos; i++) {
                     valuesArray.push([lastDepartamentoId,partidosId[i],nombreCandidatos[i]])         
               }     
               
               const sqlregistrareleccion = `INSERT INTO tbl_candidatos(
                  partidoId,
                  departamentoId,
                  nombreCandidato
                  )
                  VALUES ?`;
               conexion.query(sqlregistrareleccion,    
               [arrayCandidatos]
               ,function(errorInsert, dataInsert){
                  if (errorInsert){ 
                     throw errorInsert;
                  }else{
                           console.log('se insertaron los Cantidatos');
                     } 
            });
         }
      });

});


//Crear y levantar el servidor web.
app.listen(3000);
console.log("Servidor iniciado");