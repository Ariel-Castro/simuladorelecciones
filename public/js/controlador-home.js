jQuery(document).ready(function(){
    var totalPartidos = 0;
    var arrayGrafico = [];
    var arrayGraficoCantidad = [];
    obtenerEleccionesGuardadas();
//crear una nueva eleccion
// function registrarEleccion(){
$('#form_IniciarEleccion').submit(function(event) {
    event.preventDefault();
    var nombresPartidos = $("input[name='txtNombrePartido[]']").map(function(){return $(this).val();}).get();
    var siglasPartidos = $("input[name='txtSiglas[]']").map(function(){return $(this).val();}).get();
    var cantidadVotos = $("input[name='txtVotos[]']").map(function(){return $(this).val();}).get();
    $.ajax({
        url: '/registrarEleccion',
		dataType:"json",
		method:"POST",
		data: {
			sistema: $('#cbxSistema').val(),
			depto: $('#cbxDepto').val(),
            cantidadDiputados: $('#txtCantidadDiputados').val(),
            nombreEleccion:  $('#txtnombreEleccion').val(),
            fecha: $('#txtFecha').val(),
			censoElectoral: $('#txtCensoElectoral').val(),
			votosBlanco: $('#txtVotosBlanco').val(),
            votosNulos: $('#txtvotosNulos').val(),
            'nombrepartidos': nombresPartidos,
            'siglasPartidos': siglasPartidos,
            'cantidadVotos': cantidadVotos
		},
		success: function(data){
            cargarCantidadDiputados();
            $("#divResultados").show();
        },
        error: function(data){
            cargarCantidadDiputados();
            $("#divResultados").show();
        }
    });

	
});


//crear una nueva eleccion
function registrarCandidatos(){
    var partidosId = $("input[name='cbxPartido[]']").map(function(){return $(this).val();}).get();
    var nombresCandidatos = $("input[name='txtNombreCandidato[]']").map(function(){return $(this).val();}).get();
    var datos = {
        'partidosId': partidosId,
        'nombreCandidatos': nombresCandidatos
    }
    
	$.ajax({
		url: '/registrarCandidatos',
		dataType:"json",
		method:"POST",
		data: datos,
		success:function(respuesta){
			alert('se añadio nuevo eleccion');
		}
	});

}

// obtener cantidad de diputados
function cargarCantidadDiputados(){
    //Esta funcion se ejecuta cuando la página esta lista
    var resultadoDiputados = 0;
    $("#tbody_resultados").html('');
	$.ajax({
		url:"/obtener-cantidad-candidatos",
		dataType:"json",
		success:function(respuesta){
            for(var i=0; i<respuesta.length-1; i++){
                resultadoDiputados += respuesta[i].CantidadDiputados
            }
            if (resultadoDiputados == $('#txtCantidadDiputados').val()) {
                for(var i=0; i<respuesta.length-1; i++){
                    $("#tbody_resultados").append(
                        `
                      <tr>
                        <td>${respuesta[i].nombrePartido}</td>
                        <td>${respuesta[i].siglasPartido}</td>
                        <td>${respuesta[i].CantidadDiputados}</td>
                      </tr>`
                    );
                    arrayGrafico[i] = respuesta[i].siglasPartido;
                    arrayGraficoCantidad[i] = respuesta[i].CantidadDiputados;
                }
                generarGrafico();
                obtenerEleccionesGuardadas();
            } else {
                for(var i=0; i<respuesta.length-1; i++){
                    $("#tbody_resultados").append(
                        `
                      <tr>
                        <td>${respuesta[i].nombrePartido}</td>
                        <td>${respuesta[i].siglasPartido}</td>
                        <td>${respuesta[i].CantidadDiputados}</td>
                      </tr>`
                    );
                    arrayGrafico[i] = respuesta[i].siglasPartido;
                    arrayGraficoCantidad[i] = respuesta[i].CantidadDiputados;
                }
                obtenerCandidatosResiduo();
                generarGrafico();
                obtenerEleccionesGuardadas();

            }

		}
	});
};

// obtener cantidad de diputados por residuo
function obtenerCandidatosResiduo(){
    var resultadoDiputados = 0;
    $("#tbody_resultadosResiduo").html('');
	$.ajax({
		url:"/obtener-cantidad-residuo",
		dataType:"json",
		success:function(respuesta){
                for(var i=0; i<respuesta.length-1; i++){
                    $("#tbody_resultadosResiduo").append(
                        `
                      <tr>
                        <td>${respuesta[i].nombrePartido}</td>
                        <td>${respuesta[i].siglasPartido}</td>
                        <td>${respuesta[i].residuo}</td>
                        <td>${respuesta[i].cantCandidatos}</td>
                      </tr>`
                    );

            }

		}
	});
};

// obtener cantidad de diputados por residuo
function obtenerEleccionesGuardadas(){
    var html = "<option value='-1'>Seleccione</option>";
	$.ajax({
		url:"/obtener-eleccion-guardadas",
		dataType:"json",
		success:function(respuesta){
                 for(var i=0; i<respuesta.length; i++){
                        // html += `<option value = "${respuesta[i].eleccionId}" >${respuesta[i].nombreEleccion}</option>`
                        html += '<option value = '+respuesta[i].eleccionId+'>'+respuesta[i].nombreEleccion+'</option>'
             }
            $("#cbxEleccionCargar").html(html);

		}
	});
};

//carga el contenido general de eleccion insertada
function cargarContenidoEleccionGuardada(eleccionId){
	//Esta funcion se ejecuta cuando la página esta lista
	$.ajax({
		url:"/obtener-datos-eleccion",
		method:"GET",
		data:"eleccionId="+eleccionId,
		dataType:"json",
		success:function(respuesta){
            $('#txtCantidadDiputados').val(respuesta[0].cantidadDiputados),
            $('#txtnombreEleccion').val(respuesta[0].nombreEleccion),
            $('#txtFecha').val(respuesta[0].fechaEleccion),
			$('#txtCensoElectoral').val(respuesta[0].censoElectoral),
			$('#txtVotosBlanco').val(respuesta[0].votosBlanco),
            $('#txtvotosNulos').val(respuesta[0].votosNulo)
		}
	});
};

//carga los partidos guardados
function cargarpartidosGuardados(eleccionId){
    //Esta funcion se ejecuta cuando la página esta lista
    $("#tbodyTabla").html('');
	$.ajax({
		url:"/obtener-datos-partidos",
		method:"GET",
		data:"eleccionId="+eleccionId,
		dataType:"json",
		success:function(respuesta){
            for(var i=0; i<respuesta.length; i++){
                totalPartidos +=1;
                $("#tbodyTabla").append(
                    `
                    <tr class="fila-fija">
                        <td><input required class="form-control" name="txtNombrePartido[]" placeholder="nombre del partido" value="${respuesta[i].nombrePartido}"/></td>
                        <td><input required  class="form-control" name="txtSiglas[]" placeholder="siglas" value="${respuesta[i].siglasPartido}"/></td>
                        <td><input required type="number" id="idVotos${i+1}" class="form-control" name="txtVotos[]" placeholder="Cantidad de Votos" value="${respuesta[i].cantidadVotos}"/></td>
                        <td class="eliminar"><input type="button" class="btn"  value="Eliminar"/></td>
                    </tr>
                    `
                )
            }
            $('#divAleatoriedad').show(100);
		}
	});
};

$("#cbxEleccionCargar").change(function(){
    var eleccionSeleccionada = $("#cbxEleccionCargar").val();

    cargarContenidoEleccionGuardada(eleccionSeleccionada);
    cargarpartidosGuardados(eleccionSeleccionada);

});

$("#cbxPais").change(function(){
    var paisSeleccionado = $("#cbxPais").val();

    if (paisSeleccionado == 1) {
        $("#divCantDip").hide(300);
        $("#divDepts").show(300);
    } else {
        $("#divDepts").hide(300);
        $("#divCantDip").show(300);
    }

});

function generarAletoriedad() {
    var censoElectoralVal = $("#txtCensoElectoral").val();
    var votosBlancosVal = $("#txtVotosBlanco").val();
    var votosnulosVal = $("#txtvotosNulos").val();
    var totalVotosValidosMax = 0;
    var numeroPartidoObt = new Array(totalPartidos);
    var numAleatorio = 0;
    var idInput = 0;
    totalVotosValidosMax = censoElectoralVal - votosBlancosVal - votosnulosVal;
    for (let i = 0; i < totalPartidos; i++) {
        numeroPartidoObt[i] = Math.floor(Math.random() * (totalPartidos-0) + 0);
        idInput = i+1
        numAleatorio = Math.floor(Math.random() * (totalVotosValidosMax-0) + 0);
        totalVotosValidosMax -= numAleatorio; 
        $("#idVotos"+idInput+"").val(numAleatorio);
    }
    

}

$("#btnGenerarAleatorios").click(function() {
    generarAletoriedad();
});

$(function(){
    // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
    $("#adicional").on('click', function(){
        totalPartidos +=1;
        $("#tbodyTabla").append(
            `
            <tr class="fila-fija">
                <td><input required class="form-control" name="txtNombrePartido[]" placeholder="nombre del partido"/></td>
                <td><input required  class="form-control" name="txtSiglas[]" placeholder="siglas"/></td>
                <td><input required  type="number" id="idVotos${totalPartidos}" class="form-control" name="txtVotos[]" placeholder="Cantidad de Votos"/></td>
                <td class="eliminar"><input type="button" class="btn"  value="Eliminar"/></td>
            </tr>
            `
        )
        $('#divAleatoriedad').show(100);
    });
 
    // Evento que selecciona la fila y la elimina 
    $(document).on("click",".eliminar",function(){
        totalPartidos -=1;
        var parent = $(this).parents().get(0);
        $(parent).remove();
        if (totalPartidos == 0) {
            $('#divAleatoriedad').hide(100);
        }
    });
});



$(function(){
    // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
    $("#adicionarCandidato").on('click', function(){
        $("#tablaCandidatos tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tablaCandidatos");
    });
 
    // Evento que selecciona la fila y la elimina 
    $(document).on("click",".eliminarCandidato",function(){
        var parent = $(this).parents().get(0);
        $(parent).remove();
    });
});


/************************ Grafico******************/

function generarGrafico() {
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        console.log('slice' + arrayGrafico.slice(0));
        
         for (let i = 0; i < arrayGrafico.length; i++) {
            
            data.addRows([[arrayGrafico[i],arrayGraficoCantidad[i]]]);
        }

        

        // Set chart options
        var options = {'title':'Representacion de Partidos ',
                       'width':550,
                       'height':450};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('grafica'));
        chart.draw(data, options);
      }
};
/** fin grafico */

});